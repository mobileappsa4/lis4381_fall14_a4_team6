# README #

Assignment 4: Tutorial 

# Bit Bucket Commands#

## 1) “git init”  ##

* Overview: Creates an empty Git repository and can also initialize one that already exists. It essentially creates a .git sub-directory within the project root. The existing project remains untouched.

![me1.PNG](https://bitbucket.org/repo/aGezXM/images/1380225989-me1.PNG)

* 1)Type “cd local_directory/gitbucket_respository/
*  EXAMPLE: cd lis4381_fall14_a4_team6/a4/

![me2.PNG](https://bitbucket.org/repo/aGezXM/images/4219827805-me2.PNG)

* 2) Type “git init” a4 is being initialized, but preexisting content 
* (in our case “bootstraptemp.html”) is not altered

![me3.PNG](https://bitbucket.org/repo/aGezXM/images/908593491-me3.PNG)

* 3) Type “ls -a” to see resulting .git subdirectory created.
* A .git subdirectory is created
* “bootstraptemp.html” is unaltered 

![me4.PNG](https://bitbucket.org/repo/aGezXM/images/1389343848-me4.PNG)

* 4) To view the contents within the .git directory command “git init” created, type “cd .git” then "ls"
*  You can see the all the metadata of the project stored inside the .git subdirectory.

## 2) “git status” ##

* Overview: Displays any changes that could be made if “git commit” was run to the project in the local machine versus the project original on Gitbucket. Also shows what has the potential to be committed if “git add” was run before “git commit”.
*   Basically, it displays what would be committed if “git commit” was run right then and what it has the potential to commit if “git add” was run before “git commit”.

![gitstatus.PNG](https://bitbucket.org/repo/aGezXM/images/3229597757-gitstatus.PNG)


* Type “git status”
* 1) Shows branch1 location and what has the potential to be committed (which in this case is nothing) .  
* Thus, the potential of change for “git add” being run before “git commit” is the same as the working directory is clean. 

## 3) “git add” ##

* Overview: The git add command adds a change in the working directory of the staging area. It tells the Git that you want to include updates to a particular file in the next commit. However, git add doesn’t really affect the repository in any significant way. The changes are not recorded officially without running the git commit command afterwards. 

![shaun1.PNG](https://bitbucket.org/repo/aGezXM/images/1946565730-shaun1.PNG)

* Create a file in your intended directory. (In this case directory name is: lis4381_fall2014_a4_team6/a4)

![shaun2.PNG](https://bitbucket.org/repo/aGezXM/images/1788233554-shaun2.PNG)

* Then use git status to make sure that the repository is aware of the new file. You can see that Git is aware that you created the file in your repository. The bottom output line then gives you a hint to the next step, where it says “nothing added to commit but untracked files present (use ‘git add’ to track)”.

![shaun3.PNG](https://bitbucket.org/repo/aGezXM/images/217726174-shaun3.PNG)

* You then must add the file like it said in the previous line. The git add command moves changes from the working directory to the staging area. This gives you the opportunity to prepare a set of changes before committing it to the history.

![shaun4.PNG](https://bitbucket.org/repo/aGezXM/images/831895532-shaun4.PNG)

* The next step would be to again use the git status command. Now you can see the new file has been added (staged) and you can commit it when you are ready. This leads into the “git commit” command.

## 4) git-commit ---- Records changes to the repository. ##
	
* Overview: The git-commit command will store the current contents of index in a new commit along with a log message from the user describing the changes.

![bobby1.PNG](https://bitbucket.org/repo/aGezXM/images/3701947761-bobby1.PNG)

* Type “git commit”
* In this case I edited the bootstrap.html file and then submitted a commit to document what I changed.
* Various options can be added to the command such as:
* -m    -   This allows the user to add a commit message which makes it easier for others to identify the change that was made.
* -s     -    This allows for a Signed-off-by line at the end of the commit log message.
* --branch   -   This will display the branch and tracking info.


## 5) “git pull” ##

* Overview: Incorporates changes from a remote repository to the current branch. git pull runs the command “git fetch” and calls “git merge” to merge the retrieved branch heads into the current branch. Essentially, it fetches from and integrates with another repository or a local branch.

![bobby22.PNG](https://bitbucket.org/repo/aGezXM/images/341215507-bobby22.PNG)

* Type “cd local_directory/gitbucket_respository/” to change to the directory in which we need to pull to.
* EXAMPLE: cd lis4381_fall14_a4_team6/a4/

![bobby3.PNG](https://bitbucket.org/repo/aGezXM/images/2682086888-bobby3.PNG)

* Type “ls” to list all current files in your local directory. This is to ensure we obtain the files we’re looking for.

![bobby4.PNG](https://bitbucket.org/repo/aGezXM/images/3490122127-bobby4.PNG)

* Type “git pull --all” to pull all files from the repository to the local files.

![bobby5.PNG](https://bitbucket.org/repo/aGezXM/images/462596976-bobby5.PNG)

* Type “ls” once more to ensure that we have added a new file to the local directory. 

## 6)”git push” ##

* Overview: Updates remote files using local files. Also updates associated objects.

![bobby6.PNG](https://bitbucket.org/repo/aGezXM/images/768562503-bobby6.PNG)

* Change to your appropriate directory

![bobby7.PNG](https://bitbucket.org/repo/aGezXM/images/3870532700-bobby7.PNG)

* Type “git push origin master” - This finds a ref that matches “master” in the source repository and updates the same ref in the origin repository.

## 7) “Additional Git commands” ##

	* 7.1) Git Log
* Overview: Shows most recent commit history. Scroll up and down through the log history with ↑ and ↓ keyboard arrows. Press Q to exit and return to the command line. 

![nick1.PNG](https://bitbucket.org/repo/aGezXM/images/207355453-nick1.PNG)

______________________________________

# How do I get set up? #

## 1) Setting up Bitbucket ## 

![bitbucket.PNG](https://bitbucket.org/repo/aGezXM/images/2089334539-bitbucket.PNG)

* a) Go to https://bitbucket.org/account/signup/
* b) Fill out the signup form according to the directions provided
* c) You can also use a Google Account
* d) Click Sign Up
* e) Validate your account by checking your email and clicking the validation link provided within the email
* f) Create a Git repository
* g) Log into your account https://bitbucket.org/account/signin/

______________________________________
## 2) Repository Creation ## 

![create.PNG](https://bitbucket.org/repo/aGezXM/images/1408167371-create.PNG)

Click Create

![creation.PNG](https://bitbucket.org/repo/aGezXM/images/3941238228-creation.PNG)

* a) Enter the name of your project (example “bb101repo”)
* b) Enter the description of your project (example “test”)
* c) Check the check mark if you wish to make your repository private
* d) Choose the privacy of your forking setting
* e) Select “Git” for repository type
* f) Check the “Issue Tracking” and “Wiki” checkboxes
* g) Set your Language to “HTML/CSS”
* h) Click “Create Repository” 

![newrepo.PNG](https://bitbucket.org/repo/aGezXM/images/3938665477-newrepo.PNG)

Congratulations! You have created your new repository! 
______________________________________________________

## 3) Setting up Git ## 

![git1.PNG](https://bitbucket.org/repo/aGezXM/images/2648251285-git1.PNG)

* Run the installer and follow the steps to complete installation.

![git2.PNG](https://bitbucket.org/repo/aGezXM/images/3160738069-git2.PNG)

* Press Next to move to the next page of the wizard. The setup displays the license agreement.
* Press Next to accept the license agreement and continue. For this setup, use all the default setup values recommended by installer.
* To accept all the defaults, press Next on each page of the dialog that comes after the license agreement.

![git3.PNG](https://bitbucket.org/repo/aGezXM/images/2130914622-git3.PNG)

* Press Finish to complete Git Bash installation.
* Close the release notes.

![git4.PNG](https://bitbucket.org/repo/aGezXM/images/1177739792-git4.PNG)

* Open the Git Bash window
* Use the following command to configure your username.
* git config --global user.name "FIRST_NAME LAST_NAME"
* Use the following command to configure your email
* git config --global user.email "MY_NAME@example.com"

______________________________________________________

# Contribution guidelines #

![Marquel Change.PNG](https://bitbucket.org/repo/aGezXM/images/2793864816-Marquel%20Change.PNG)

### Marquel_Nakashima: ###

* Tutorial Contribution: Commands "git init," "git status," Setting up Bitbucket, Repository Creation, compiled README
* Change: (indicated by screen shot)
* Marquel Nakashima:  mnn09c@my.fsu.edu

---------------------------------------------------------
### Shaun_Enriquez: ###

Tutorial Contribution: Command "git add"
Bootstrap Temp Change: added h3 heading that included two sentences
SSH generation

Shaun Enriquez: spe12@my.fsu.edu

---------------------------------------------------------
### Robert Patterson ###

Tutorial Contribution: Commands "git push" and "git pull"
Bootstrap Temp Change: Changed the navbar to assignment 4

Robert Patterson: rcp11h@my.fsu.edu

---------------------------------------------------------
### Ryan Fuhrman ###

Tutorial Contribution: Command "git commit"
Bootstrap Temp Change: Changed the first paragraph to display our choice of a Bootstrap Template.

Ryan Fuhrman: rsf13@my.fsu.edu

---------------------------------------------------------
### Nick Segal ###

Repository: Repo Creation
Tutorial Contribution: Additional Git Commands
Bootstrap Temp Change: Added an H4 element with some carefully chosen text. 

Nick Segal: nas11h@my.fsu.edu